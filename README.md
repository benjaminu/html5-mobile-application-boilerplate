### Boilerplate PhoneGap App with RequireJS, Backbone and jQuery Mobile

This repo was cloned from [appboil-requirejs-backbonejs-jquerymobile-phonegap](https://github.com/rodrigobarona/appboil-requirejs-backbonejs-jquerymobile-phonegap)

Latest release includes following libs:

* [RequireJS](http://requirejs.org/) 2.0.6 (with domReady 2.0.1 and text 2.0.10 plugins)
* [Backbone.js](http://backbonejs.org/) 1.0.0
* [Underscore.js](http://underscorejs.org/) 1.5.2
* [jQuery](http://jquery.com/) 1.9.1
* [jQuery Mobile](http://jquerymobile.com/) 1.3.2
* [jqmNavigator](https://github.com/pwalczyszyn/jqmNavigator) 2.0.0 (jQuery Mobile plugin)
* [PhoneGap](http://phonegap.com/) 3.0.0

***When using PhoneGap Build, delete the ```scripts/libs/cordova``` directory, and the PhoneGapJS section in the ```index.html``` file***