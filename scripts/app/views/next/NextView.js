/**
 * Created by Piotr Walczyszyn (outof.me | @pwalczyszyn)
 * Adapted by Benjamin Ugbene (@benjaminu)
 */

define(
    ['underscore', 'Backbone', 'text!tpl/next/NextView.tpl'],
    function (_, Backbone, NextViewTemplate) {
        var NextView = Backbone.View.extend({

            events:{
                'click #btnBack':'btnBack_clickHandler'
            },

            render:function () {
                this.$el.html(_.template(NextViewTemplate));
                return this;
            },

            btnBack_clickHandler:function (event) {
                $.mobile.jqmNavigator.popView();
            }
        });

        return NextView;
    }
);