/**
 * Created by Piotr Walczyszyn (outof.me | @pwalczyszyn)
 * Adapted by Benjamin Ugbene (@benjaminu)
 */

define(
    ['jquery', 'underscore', 'Backbone', 'app/views/next/NextView', 'text!tpl/home/HomeView.tpl'],
    function ($, _, Backbone, NextView, HomeViewTemplate) {
        var HomeView = Backbone.View.extend({

            events:{
                'click #btnNextView':'btnNextView_clickHandler'
            },

            render:function () {
                this.$el.html(_.template(HomeViewTemplate));
                return this;
            },

            btnNextView_clickHandler:function (event) {
                $.mobile.jqmNavigator.pushView(new NextView);
            }
        });

        return HomeView;
    }
);