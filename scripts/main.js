/**
 * Created by Piotr Walczyszyn (@pwalczyszyn)
 * Adapted by Benjamin Ugbene (@benjaminu)
 */

require.config({
    baseUrl: 'scripts',

    paths: {
        // App path
        app: 'app',
        // Templates directory
        tpl: 'templates',
        // RequireJS plugin
        text:'libs/require/text',
        // RequireJS plugin
        domReady:'libs/require/domReady',
        // underscore library
        underscore:'libs/underscore/underscore-min',
        // Backbone.js library
        Backbone:'libs/backbone/backbone-min',
        // jQuery
        jquery:'libs/jquery/jquery.min',
        // jQuery Mobile framework
        jqm:'libs/jquery.mobile/jquery.mobile.min',
        // jQuery Mobile plugin for Backbone views navigation
        jqmNavigator:'libs/jquery.mobile/jqmNavigator-min'
    },

    shim:{
        Backbone:{
            deps:['underscore', 'jquery'],
            exports:'Backbone'
        },
        underscore:{
            exports:'_'
        },
        jqm:{
            deps:['jquery', 'jqmNavigator']
        }
    }
});

require(['domReady', 'app/views/home/HomeView', 'jqm'],
    function (domReady, HomeView) {
        // domReady is RequireJS plugin that triggers when DOM is ready
        domReady(function () {
            function onDeviceReady(desktop) {
                // Hiding splash screen when app is loaded
                if (desktop !== true) {
                    cordova.exec(null, null, 'SplashScreen', 'hide', []);
                }

                // Setting jQM pageContainer to #container div, this solves some jQM flickers & jumps
                // See: http://outof.me/fixing-flickers-jumps-of-jquery-mobile-transitions-in-phonegap-apps/
                $.mobile.pageContainer = $('#container');

                // Setting default transition to slide
                $.mobile.defaultPageTransition = 'slide';

                // Pushing MainView
                $.mobile.jqmNavigator.pushView(new HomeView());
            }

            if (navigator.userAgent.match(/(iPad|iPhone|Android)/)) {
                // This is running on a device so waiting for deviceready event
                document.addEventListener('deviceready', onDeviceReady, false);
            } else {
                // On desktop don't have to wait for anything
                onDeviceReady(true);
            }
        });
    });